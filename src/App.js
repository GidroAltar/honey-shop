import React, { Component } from 'react'
import {
  Route,
  Link,
  Switch
} from 'react-router-dom'
import { connect } from 'react-redux'
import * as cuba from '@cuba-platform/rest'

import { HoneyAPI } from './serverAPIService'
import Order from './components/Order'
import Honey from './components/Honey'
import Consumer from './components/Consumer'
import Provider from './components/Provider'
import { initServerApp } from './actions/commonActions'
import { addHoney } from './actions/honeyActions'

import './App.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import 'jquery/dist/jquery.slim.min.js'



class App extends Component {

  componentDidMount() {
    console.log("App Did mount");
    
    
  }

  render() {
    console.log("App start render");
    const { match } = this.props;
    return (
      <div>
        <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
          <Link className="navbar-brand" to={match.path}>Мед башкирии</Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault"
            aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to="/honeys" className="nav-link">
                  Товары
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/orders" className="nav-link">
                  Заказы
                  </Link>
              </li>
              <li className="nav-item">
                <Link to="/providers" className="nav-link">
                  Поставщики
                  </Link>
              </li>
              <li className="nav-item">
                <Link to="/consumers" className="nav-link">
                  Заказчики
                  </Link>
              </li>
            </ul>
          </div>
        </nav>
        <main role="main" className="container">
          <Switch>
            <Route path="/honeys" component={Honey} />
            <Route path="/orders" component={Order} />
            <Route path="/consumers" component={Consumer} />
            <Route path="/providers" component={Provider} />
          </Switch>
        </main>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    addHoney: (honey) => dispatch(addHoney(honey))
  }
}

function mapStateToProps(state) {
  return {
    appLoaded: state.common.appLoaded
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

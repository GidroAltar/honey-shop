import { ActionCreator, Action, ActionCreatorsMapObject } from "redux"
import { Dispatch } from "react-redux";
import {
    ADD_CONSUMER,
    SAVE_CONSUMER,
    DELETE_CONSUMER
} from "../constants/actionTypes"

export const addConsumer = (consumer) => ({
    type: ADD_CONSUMER,
    payload: {
        value: {
            consumer: consumer
        }
    }
});

export const saveConsumer = (consumer) => ({
    type: SAVE_CONSUMER,
    payload: {
        value: {
            consumer: consumer
        }
    }
});

export const deleteConsumer = (consumer) => ({
    type: DELETE_CONSUMER,
    payload: {
        value: {
            consumer: consumer
        }
    }
})

export const consumerActionCreators = {
    addConsumer: addConsumer,
    saveConsumer: saveConsumer,
    deleteConsumer: deleteConsumer,
}

export default consumerActionCreators;


/*
Add Consumer
{
    type: 'ADD_CONSUMER',
        payload: {
            value: {
                consumer: { id: 1024, name: "testConsumerName", description: "description" }
            }
    }
}

Save Consumer
{
    type: 'SAVE_CONSUMER',
        payload: {
        value: { consumer: { id: 1024, name: "testConsumerNameModified", description: "description" } }
    }
}

Delete Consumer
{
    type: 'DELETE_CONSUMER',
        payload: {
        value: { consumer: { id: 1024, name: "testConsumerName", description: "description" } }
    }
}
*/
import { ActionCreator, Action, ActionCreatorsMapObject } from "redux"
import { Dispatch } from "react-redux";
import { 
    ADD_ORDER,
    SAVE_ORDER,
    DELETE_ORDER
}  from "../constants/actionTypes"


export const addOrder = (order) => ({
    type: ADD_ORDER,
    payload: {
        value: {
            order: order
        }
    }
});

export const saveOrder = (order) => ({
    type: SAVE_ORDER,
    payload: {
        value: {
            order: order
        }
    }
});

export const deleteOrder = (order) => ({
    type: DELETE_ORDER,
    payload: {
        value: {
            order: order
        }
    }
})

export const orderActionCreators = {
    addOrder: addOrder,
    saveOrder: saveOrder,
    deleteOrder: deleteOrder,
}

export default orderActionCreators;


/*
Add Order
{
    type: 'ADD_ORDER',
        payload: {
            value: {
                order: { id: 1024, name: "testOrderName", description: "description", startDate: "21-03-20017", status: "выполняется" }
            }
    }
}

Save Order
{
    type: 'SAVE_ORDER',
        payload: {
        value: { order: { id: 1024, name: "testOrderNameModified", description: "description" startDate: "21-03-20017", status: "выполняется" } }
    }
}

Delete Order
{
    type: 'DELETE_ORDER',
        payload: {
        value: { order: { id: 1024, name: "testOrderName", description: "description" startDate: "21-03-20017", status: "выполняется" } }
    }
}
*/
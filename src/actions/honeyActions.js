import { ActionCreator, Action, ActionCreatorsMapObject } from "redux"
import { Dispatch } from "react-redux";
import { HoneyAPI } from "../serverAPIService";

import {
    ADD_HONEY,
    SAVE_HONEY,
    DELETE_HONEY,
    RECEIVE_HONEY,
    RECEIVE_HONEY_START,
    RECEIVE_HONEY_END
} from "../constants/actionTypes";

export const addHoney = (honey) => ({
    type: ADD_HONEY,
    payload: {
        value: {
            honey: honey
        }
    }
});

export const saveHoney = (honey) => ({
    type: SAVE_HONEY,
    payload: {
        value: {
            honey: honey
        }
    }
});

export const deleteHoney = (honey) => ({
    type: DELETE_HONEY,
    payload: {
        value: {
            honey: honey
        }
    }
})

function receiveHoney(honeyList) {
    return {
        type: RECEIVE_HONEY,
        payload: {
            value: {
                honey: honeyList
            }
        }
    }
}

export function fetchHoney() {
    return (dispatch, getState) => {
        dispatch({ type: RECEIVE_HONEY_START });
        const { serverApp } = getState().common;
        return HoneyAPI.getAll(serverApp)
            .then(honeyList => {
                console.log(honeyList);
                dispatch(receiveHoney(honeyList));
            })
            .then(() => {dispatch({ type: RECEIVE_HONEY_END })})
    }
}

export function createHoney(honey) {
    return (dispatch, getState) => {
        const { serverApp } = getState().common;
        return HoneyAPI.post(honey, serverApp)
            .then(response => {
                console.log(response);
            })
    }
}

export const honeyActionCreators = {
    addHoney: addHoney,
    saveHoney: saveHoney,
    deleteHoney: deleteHoney,
    fetchHoney: fetchHoney,
}

export default honeyActionCreators;


/*
Add Honey
{
    type: 'ADD_HONEY',
        payload: {
            value: {
                honey: { id: 1024, name: "testHoneyName", description: "description" }
            }
    }
}

Save Honey
{
    type: 'SAVE_HONEY',
        payload: {
        value: { honey: { id: 1024, name: "testHoneyNameModified", description: "description" } }
    }
}

Delete Honey
{
    type: 'DELETE_HONEY',
        payload: {
        value: { honey: { id: 1024, name: "testHoneyName", description: "description" } }
    }
}
*/
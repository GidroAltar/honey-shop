import { ActionCreator, Action, ActionCreatorsMapObject } from "redux"
import { Dispatch } from "react-redux";
import {
    ADD_PROVIDER,
    SAVE_PROVIDER,
    DELETE_PROVIDER
} from "../constants/actionTypes"

export const addProvider = (provider) => ({
    type: ADD_PROVIDER,
    payload: {
        value: {
            provider: provider
        }
    }
});

export const saveProvider = (provider) => ({
    type: SAVE_PROVIDER,
    payload: {
        value: {
            provider: provider
        }
    }
});

export const deleteProvider = (provider) => ({
    type: DELETE_PROVIDER,
    payload: {
        value: {
            provider: provider
        }
    }
})

export const providerActionCreators = {
    addProvider: addProvider,
    saveProvider: saveProvider,
    deleteProvider: deleteProvider,
}

export default providerActionCreators;


/*
Add Provider
{
    type: 'ADD_PROVIDER',
        payload: {
            value: {
                provider: { id: 1024, name: "testProviderName", description: "description" }
            }
    }
}

Save Provider
{
    type: 'SAVE_PROVIDER',
        payload: {
        value: { provider: { id: 1024, name: "testProviderNameModified", description: "description" } }
    }
}

Delete Provider
{
    type: 'DELETE_PROVIDER',
        payload: {
        value: { provider: { id: 1024, name: "testProviderName", description: "description" } }
    }
}
*/
import { ActionCreator, Action, ActionCreatorsMapObject } from "redux"
import { Dispatch } from "react-redux";
import {
    SERVER_APP_INIT,
    CONFIRM_RECEIVE_HONEY
} from "../constants/actionTypes"

export const initServerApp = (app) => ({
    type: SERVER_APP_INIT,
    payload: {
        value: {
            app: app
        }
    }
});

export const commonActionCreators = {
    initServerApp: initServerApp
}

export default commonActionCreators;
import {ADD_ORDER, SAVE_ORDER, DELETE_ORDER} from "../constants/actionTypes";

const orderReducer = (orders = [], action) => {
    switch (action.type) {
        case ADD_ORDER: 
            return [...orders, action.payload.value.order];
        case DELETE_ORDER:
            return orders.filter((iterOrder) => iterOrder.id !== action.payload.value.order.id);
        case SAVE_ORDER:
            return orders.map((iterOrder) => (iterOrder.id === action.payload.value.order.id) ? action.payload.value.order : iterOrder)
        default:
            return orders
    }
}

export default orderReducer;
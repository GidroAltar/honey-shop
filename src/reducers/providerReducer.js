import {ADD_PROVIDER, SAVE_PROVIDER, DELETE_PROVIDER} from "../constants/actionTypes";

const providerReducer = (providers = [], action) => {
    switch (action.type) {
        case ADD_PROVIDER: 
            return [...providers, action.payload.value.provider];
        case DELETE_PROVIDER:
            return providers.filter((iterProvider) => iterProvider.id !== action.payload.value.provider.id);
        case SAVE_PROVIDER:
            return providers.map((iterProvider) => (iterProvider.id === action.payload.value.provider.id) ? action.payload.value.provider : iterProvider)
        default:
            return providers
    }
}

export default providerReducer;
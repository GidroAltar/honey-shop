import { combineReducers } from "redux"
import { reducer as formReducer } from 'redux-form'

import entities from "./entities"
import commonReducer from './commonReducer'
import pages from './pages/pages'

export default combineReducers(
  {
    common: commonReducer,
    entities: entities,
    pages: pages,
    form: formReducer
  }
)
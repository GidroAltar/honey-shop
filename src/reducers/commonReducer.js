import {APP_LOAD, SERVER_APP_INIT} from "../constants/actionTypes";

const defaultState = {
    appLoaded: false,
    serverApp: null
}

const commonReducer = (state = defaultState, action) => {
    switch (action.type) {
        case APP_LOAD: 
            return state;
        case SERVER_APP_INIT: 
            return {...state, serverApp: action.payload.value.app};
        default: 
            return state
    }
}

export default commonReducer;
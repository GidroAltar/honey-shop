import { ADD_HONEY, SAVE_HONEY, DELETE_HONEY, RECEIVE_HONEY } from "../constants/actionTypes";
import { arrayToObject } from "../utils";

const honeyReducer = (honey = {}, action) => {
    switch (action.type) {
        case ADD_HONEY:
            return {};
        case DELETE_HONEY: {
            let { [action.payload.value.honey.id]: objectToDelete, ...newHoneyCollection } = honey;
            return newHoneyCollection;
        }
        case SAVE_HONEY:
            return {...honey, [action.payload.value.honey.id]: action.payload.value.honey};
        case RECEIVE_HONEY:
            return arrayToObject(action.payload.value.honey, "id");
        default:
            return honey
    }
}

export default honeyReducer;
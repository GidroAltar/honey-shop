import {ADD_CONSUMER, SAVE_CONSUMER, DELETE_CONSUMER} from "../constants/actionTypes";

const consumerReducer = (consumers = [], action) => {
    switch (action.type) {
        case ADD_CONSUMER: 
            return [...consumers, action.payload.value.consumer];
        case DELETE_CONSUMER:
            return consumers.filter((iterConsumer) => iterConsumer.id !== action.payload.value.consumer.id);
        case SAVE_CONSUMER:
            return consumers.map((iterConsumer) => (iterConsumer.id === action.payload.value.consumer.id) ? action.payload.value.consumer : iterConsumer)
        default:
            return consumers
    }
}

export default consumerReducer;
import { combineReducers } from "redux"

import honeyReducer from "./honeyReducer"
import orderReducer from "./orderReducer"
import providerRdeucer from './providerReducer'
import consumerReducer from './consumerReducer'

export default combineReducers(
  {
      honey: honeyReducer,
      orders: orderReducer,
      providers: providerRdeucer,
      consumers: consumerReducer
  }
)
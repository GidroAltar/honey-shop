import { RECEIVE_HONEY_START, RECEIVE_HONEY_END} from "../../constants/actionTypes";

const honeyPageReducer = (honey = {}, action) => {
    switch (action.type) {
        case RECEIVE_HONEY_START:
            return { ...honey, isFetching: true };
        case RECEIVE_HONEY_END:
            return { ...honey, isFetching: false };
        default:
            return honey
    }
}

export default honeyPageReducer;
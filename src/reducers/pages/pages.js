import { combineReducers } from "redux"

import honeyPageReducer from "./honey"

export default combineReducers(
  {
      honey: honeyPageReducer
  }
)
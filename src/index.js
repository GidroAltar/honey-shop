import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import { composeWithDevTools } from 'redux-devtools-extension';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import './index.css';
import App from './App';
import mainReducer from "./reducers/index";
import registerServiceWorker from './registerServiceWorker';

import * as cuba from '@cuba-platform/rest';

let app = getServerApp();
const initState = {
  common: {
    serverApp: app
  }
}
const store = createStore(mainReducer, initState, composeWithDevTools(applyMiddleware(ReduxThunk)));

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();

function getServerApp() {
  //REST server init
  const app = cuba.initializeApp({
    name: 'myApp',
    apiUrl: 'http://localhost:8080/app/rest/'
  });

  return cuba.getApp('myApp');
}

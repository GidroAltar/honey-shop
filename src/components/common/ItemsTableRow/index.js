import React, { Component } from 'react';
import PropTypes from 'prop-types';

import "./ItemsTableRow.css";

class ItemsTableRow extends Component {
  render() {
    const { fieldValues, itemActionElements } = this.props;
    return (
      <div className="row items__item">
      {
        fieldValues.map(
          field => <div key={field} className="col-sm">{ field }</div>
        )
      }
        <div className="col-sm items__actions">
        {
          itemActionElements
        }
        </div>
      </div>
    );
  }
}

ItemsTableRow.propTypes = {
  fieldValues: PropTypes.arrayOf(PropTypes.string),
  itemActionElements: PropTypes.arrayOf(PropTypes.element),
};

export default ItemsTableRow;
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import honeyActionCreators from '../../../actions/honeyActions';

import "./EditWindow.css";

class EditWindow extends Component {

    handleModalParentOnClick = (e) => {
        if (e.target === e.currentTarget) {
            this.props.handleClose();
        }
    }

    render() {
        const { windowTitle, windowBody, windowActions } = this.props;
        return ReactDOM.createPortal(
            <div>
                <div className="overlay">
                </div>
                <div className="modal-window-parent" onClick={this.handleModalParentOnClick}>
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                {windowTitle}
                            </div>
                            <div className="modal-body">
                                {windowBody}
                            </div>
                            <div className="modal-footer">
                                {windowActions}
                            </div>
                        </div>
                    </div>
                </div>
            </div>,
            document.getElementById("root")
        );
    }
}

EditWindow.propTypes = {
    windowTitle: PropTypes.string,
    windowBody: PropTypes.element,
    windowActions: PropTypes.arrayOf(PropTypes.element),
    handleClose: PropTypes.func
};

export default EditWindow;
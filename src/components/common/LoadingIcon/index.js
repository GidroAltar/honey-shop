import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import honeyActionCreators from '../../../actions/honeyActions';

import "./LoadIcon.css";

function LoadIcon() {
    return ReactDOM.createPortal(
        <div>
            <div className="overlay d-flex justify-content-center align-items-center">
                <div className="overlay-label">Loading, please wait...</div>
            </div>
        </div>,
        document.getElementById("root")
    );
}

export default LoadIcon;
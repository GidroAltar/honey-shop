import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ProviderEditWindow from '../ProviderEditWindow';
import ItemsTableRow from '../../common/ItemsTableRow';
import "./ProviderRow.css";

class ProviderRow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            providerEditWindowVisible: false
        }
    }

    hideProviderEditWindow = () => {
        this.setState({
            providerEditWindowVisible: false
        })
    }

    showProviderEditWindow = () => {
        this.setState({
            providerEditWindowVisible: true
        })
    }

    render() {
        const { providerEditWindowVisible } = this.state;
        const { provider, deleteProvider, saveProvider } = this.props;

        const providerFieldValues = [provider.name];
        const itemActionElements = [
            <button type="button" className="btn btn-primary provider-items__action" onClick={this.showProviderEditWindow}>Редактировать</button>,
            <button type="button" className="btn btn-danger provider-items__action" onClick={() => deleteProvider(provider)}>Удалить</button>
        ];
        return (
            <div>
                <ItemsTableRow
                    fieldValues={providerFieldValues}
                    itemActionElements={itemActionElements}
                />
                {
                    (providerEditWindowVisible)
                        ? <ProviderEditWindow
                            provider={provider}
                            handleClose={this.hideProviderEditWindow}
                            handleDelete={deleteProvider}
                            handleSave={saveProvider} />
                        : null
                }
            </div>
        );
    }
}

ProviderRow.propTypes = {
    provider: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string,
    }),
    deleteProvider: PropTypes.func.isRequired,
    saveProvider: PropTypes.func.isRequired,
};

export default ProviderRow;
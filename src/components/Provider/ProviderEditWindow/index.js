import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import providerActionCreators from '../../../actions/providerActions';
import EditWindow from '../../common/EditWindow';

class ProviderEditWindow extends Component {

  constructor(props) {
    super(props);
    this.state = {
        provider: props.provider
    }
  }

  handleModalParentOnClick = (e) => {
    if (e.target === e.currentTarget) {
      this.props.handleClose();
    }
  }

  handleNameOnChange = (e) => {
    this.setState( {
      provider: {...this.state.provider, name: e.currentTarget.value }
    })
  }

  handleEmailOnChange = (e) => {
    this.setState( {
      provider: {...this.state.provider, email: e.currentTarget.value }
    })
  }

  handleDescriptionOnChange = (e) => {
    this.setState( {
      provider: {...this.state.provider, description: e.currentTarget.value }
    })
  }

  handleTelephoneOnChange = (e) => {
    this.setState( {
      provider: {...this.state.provider, telephone: e.currentTarget.value }
    })
  }
  
  render() {
    const { handleDelete, handleSave, handleClose } = this.props;
    const { provider } = this.state;

    const providerEditWindowBody = <form>
    <div className="form-group">
      <label htmlFor="provider-name" className="col-form-label">Название:</label>
      <input type="text" className="form-control" id="provider-name" value = {provider.name} onChange={ this.handleNameOnChange }></input>
    </div>
    <div className="form-group">
      <label htmlFor="description-text" className="col-form-label">Описание:</label>
      <textarea className="form-control" id="description-text" value = {provider.description} onChange={ this.handleDescriptionOnChange }></textarea>
    </div>
  </form>;

  const providerEditWindowActions = [
    <button key = "1" type="button" className="btn btn-primary" onClick={ () => handleDelete(provider) } >Удалить</button>,
    <button key = "2" type="button" className="btn btn-secondary" onClick={ () => { handleSave(provider); handleClose()} } >Сохранить</button>
  ];

  return (
    <EditWindow
      windowTitle={provider.name}
      windowBody={providerEditWindowBody}
      windowActions={providerEditWindowActions}
      handleClose={handleClose}
    />
    );
  }
}

ProviderEditWindow.propTypes = {
  provider: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
  }),
  handleDelete: PropTypes.func.isRequired,
  handleSave: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default ProviderEditWindow;
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import providerActionCreators from '../../../actions/providerActions';
import EditWindow from '../../common/EditWindow';

class ProviderAddWindow extends Component {

  constructor(props) {
    super(props);
    this.state = {
        provider: {
          name: "",
          description: "",
          email: "",
          telephone: ""
        }
    }
  }

  handleModalParentOnClick = (e) => {
    if (e.target === e.currentTarget) {
      this.props.handleClose();
    }
  }

  handleNameOnChange = (e) => {
    this.setState( {
      provider: {...this.state.provider, name: e.currentTarget.value }
    })
  }

  handleEmailOnChange = (e) => {
    this.setState( {
      provider: {...this.state.provider, email: e.currentTarget.value }
    })
  }

  handleDescriptionOnChange = (e) => {
    this.setState( {
      provider: {...this.state.provider, description: e.currentTarget.value }
    })
  }

  handleTelephoneOnChange = (e) => {
    this.setState( {
      provider: {...this.state.provider, telephone: e.currentTarget.value }
    })
  }

  render() {
    const { handleAdd, handleClose } = this.props;
    const { provider } = this.state;

    const providerEditWindowBody = <form>
    <div className="form-group">
      <label htmlFor="provider-name" className="col-form-label">Название:</label>
      <input type="text" className="form-control" id="provider-name" value = {provider.name} onChange={ this.handleNameOnChange }></input>
    </div>
    <div className="form-group">
      <label htmlFor="provider-email" className="col-form-label">Статус:</label>
      <input type="text" className="form-control" id="provider-email" value = {provider.email} onChange={ this.handleEmailOnChange }></input>
    </div>
    <div className="form-group">
      <label htmlFor="provider-telephone" className="col-form-label">Описание:</label>
      <textarea className="form-control" id="provider-telephone" value = {provider.telepgone} onChange={ this.handleTelephoneOnChange }></textarea>
    </div>
    <div className="form-group">
      <label htmlFor="provider-description" className="col-form-label">Телефон:</label>
      <textarea className="form-control" id="provider-description" value = {provider.description} onChange={ this.handleDescriptionOnChange }></textarea>
    </div>
  </form>;

  const providerEditWindowActions = [
    <button key = "1" type="button" className="btn btn-secondary" onClick={ () => { handleAdd(provider); handleClose()} } >Сохранить</button>
  ];

  return (
    <EditWindow
      windowTitle={"Заполните поля:"}
      windowBody={providerEditWindowBody}
      windowActions={providerEditWindowActions}
      handleClose={handleClose}
    />
    );
  }
}

ProviderAddWindow.propTypes = {
  handleAdd: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default ProviderAddWindow;
import React, { Component } from 'react';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import ProviderRow from './ProviderRow';
import ProviderAddWindow from './ProviderAddWindow';
import providerActionCreators from '../../actions/providerActions';

import "./Provider.css";

class Provider extends Component {

  constructor(props) {
    super(props);
    this.state = {
      providerAddWindowVisible: false
    }
  }

  hideProviderAddWindow = () => {
    this.setState({
      providerAddWindowVisible: false
    })
  }

  showProviderAddWindow = () => {
    this.setState({
      providerAddWindowVisible: true
    })
  }

  render() {
    const { addProvider, saveProvider, deleteProvider } = this.props.providerActionCreators;
    const { providerAddWindowVisible } = this.state;
    return (
      <main role="main" className="container">
        {
          (providerAddWindowVisible)
            ? <ProviderAddWindow
              handleClose={this.hideProviderAddWindow}
              handleAdd={addProvider} />
            : null
        }
        <p>
          <button type="button" className="btn btn-outline-primary" onClick={this.showProviderAddWindow}>Добавить</button>
        </p>
        {this.props.providers.map((iterProvider) => <ProviderRow key={iterProvider.id} provider={iterProvider} deleteProvider={deleteProvider} saveProvider={saveProvider} />)}
      </main>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    providerActionCreators: bindActionCreators(providerActionCreators, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    providers: state.providers
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Provider);
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import honeyActionCreators from '../../../actions/honeyActions';
import EditWindow from '../../common/EditWindow';

class HoneyAddWindow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      honey: {
        name: "",
        description: "",
        status: "",
        price: 0,
        orderId: ""
      }
    }
  }

  handleModalParentOnClick = (e) => {
    if (e.target === e.currentTarget) {
      this.props.handleClose();
    }
  }

  handleNameOnChange = (e) => {
    this.setState({
      honey: { ...this.state.honey, name: e.currentTarget.value }
    })
  }

  handleStatusOnChange = (e) => {
    this.setState({
      honey: { ...this.state.honey, status: e.currentTarget.value }
    })
  }

  handleDescriptionOnChange = (e) => {
    this.setState({
      honey: { ...this.state.honey, description: e.currentTarget.value }
    })
  }

  handlePriceOnChange = (e) => {
    this.setState({
      honey: { ...this.state.honey, price: e.currentTarget.value }
    })
  }

  render() {
    const { handleAdd, handleClose } = this.props;
    const { honey } = this.state;

    const honeyEditWindowBody = <form>
      <div className="form-group">
        <label htmlFor="honey-name" className="col-form-label">Название:</label>
        <input type="text" className="form-control" id="honey-name" value={honey.name} onChange={this.handleNameOnChange}></input>
      </div>
      <div className="form-group">
        <label htmlFor="honey-status" className="col-form-label">Статус:</label>
        <input type="text" className="form-control" id="honey-status" value={honey.status} onChange={this.handleStatusOnChange}></input>
      </div>
      <div className="form-group">
        <label htmlFor="honey-price" className="col-form-label">Цена:</label>
        <textarea className="form-control" id="honey-price" value={honey.price} onChange={this.handlePriceOnChange}></textarea>
      </div>
      <div className="form-group">
        <label htmlFor="description-text" className="col-form-label">Описание:</label>
        <textarea className="form-control" id="description-text" value={honey.description} onChange={this.handleDescriptionOnChange}></textarea>
      </div>
    </form>;

    const honeyEditWindowActions = [
      <button key="1" type="button" className="btn btn-secondary" onClick={() => { handleAdd(honey); handleClose() }} >Сохранить</button>
    ];

    return (
      <EditWindow
        windowTitle={"Заполните поля:"}
        windowBody={honeyEditWindowBody}
        windowActions={honeyEditWindowActions}
        handleClose={handleClose}
      />
    );
  }
}

HoneyAddWindow.propTypes = {
  handleAdd: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default HoneyAddWindow;
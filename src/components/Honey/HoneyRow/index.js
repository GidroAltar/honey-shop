import React, { Component } from 'react';
import PropTypes from 'prop-types';

import HoneyEditWindow from '../HoneyEditWindow';
import ItemsTableRow from '../../common/ItemsTableRow';

class HoneyRow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            honeyEditWindowVisible: false
        }
    }

    hideHoneyEditWindow = () => {
        this.setState({
            honeyEditWindowVisible: false
        })
    }

    showHoneyEditWindow = () => {
        this.setState({
            honeyEditWindowVisible: true
        })
    }

    handleDeleteHoney = () => {
        const { honey, deleteHoney, saveHoney } = this.props;
        deleteHoney(honey);
    }


    render() {
        const { honeyEditWindowVisible } = this.state;
        const { honey, deleteHoney, saveHoney } = this.props;

        const honeyFieldValues = [honey.name];
        const itemActionElements = [
            <button key={1} type="button" className="btn btn-primary items__action" onClick={this.showHoneyEditWindow}>Редактировать</button>,
            <button key={2} type="button" className="btn btn-danger items__action" onClick={this.handleDeleteHoney}>Удалить</button>
        ];

        return (
            <div>
                <ItemsTableRow
                    fieldValues={honeyFieldValues}
                    itemActionElements={itemActionElements}
                />
                {
                    (honeyEditWindowVisible)
                        ? <HoneyEditWindow
                            honey={honey}
                            handleClose={this.hideHoneyEditWindow}
                            handleDelete={deleteHoney}
                            handleSave={saveHoney} />
                        : null
                }
            </div>
        );
    }
}

HoneyRow.propTypes = {
    honey: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string,
    }),
    deleteHoney: PropTypes.func.isRequired,
    saveHoney: PropTypes.func.isRequired,
};

export default HoneyRow;
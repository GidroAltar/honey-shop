import React, { Component } from 'react';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import HoneyRow from './HoneyRow';
import HoneyAddWindow from './HoneyAddWindow';
import LoadingIcon from '../common/LoadingIcon';
import honeyActionCreators from '../../actions/honeyActions';
import { HoneyAPI } from '../../serverAPIService';

import "./Honey.css";

class Honey extends Component {

  constructor(props) {
    super(props);
    this.state = {
      honeyAddWindowVisible: false
    }
  }

  componentDidMount() {
    console.log("honey did mount");
    const { fetchHoney } = this.props.honeyActionCreators;
    fetchHoney();
  }

  hideHoneyAddWindow = () => {
    this.setState({
      honeyAddWindowVisible: false
    })
  }

  showHoneyAddWindow = () => {
    this.setState({
      honeyAddWindowVisible: true
    })
  }

  render() {
    console.log("Honey start render");
    const { createHoney, saveHoney, deleteHoney, fetchHoney } = this.props.honeyActionCreators;
    const { honeyAddWindowVisible } = this.state;
    const { honey, isFetching } = this.props;
    return (
      <main role="main" className="container">
        {
          (honeyAddWindowVisible)
            ? <HoneyAddWindow
              handleClose={this.hideHoneyAddWindow}
              handleAdd={createHoney} />
            : null
        }
        {
          (isFetching)
            ? <LoadingIcon/>
            : null
        }
        <p>
          <button type="button" className="btn btn-outline-primary" onClick={this.showHoneyAddWindow}>Добавить</button>
        </p>
        {
          honey && Object.keys(honey).map(honeyId => {
            const iterHoney = honey[honeyId];
            return <HoneyRow
              key={iterHoney.id}
              honey={iterHoney}
              deleteHoney={deleteHoney}
              saveHoney={saveHoney}
            />
          }
          )
        }
      </main>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    honeyActionCreators: bindActionCreators(honeyActionCreators, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    serverApp: state.common.serverApp,
    honey: state.entities.honey,
    isFetching: state.pages.honey.isFetching
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Honey);
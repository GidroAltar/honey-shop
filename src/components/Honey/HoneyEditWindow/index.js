import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux"
import { Field, reduxForm } from 'redux-form'

import honeyActionCreators from '../../../actions/honeyActions'
import EditWindow from '../../common/EditWindow'

class HoneyEditWindow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      honey: props.honey
    }
  }

  handleModalParentOnClick = (e) => {
    if (e.target === e.currentTarget) {
      this.props.handleClose();
    }
  }

  handleNameOnChange = (e) => {
    this.setState({
      honey: { ...this.state.honey, name: e.currentTarget.value }
    })
  }

  handleDescriptionOnChange = (e) => {
    this.setState({
      honey: { ...this.state.honey, description: e.currentTarget.value }
    })
  }

  handlePriceOnChange = (e) => {
    this.setState({
      honey: { ...this.state.honey, price: e.currentTarget.value }
    })
  }

  handleStatusOnChange = (e) => {
    this.setState({
      honey: { ...this.state.honey, status: e.currentTarget.value }
    })
  }

  render() {
    const { handleDelete, handleSave, handleClose } = this.props;
    const { honey } = this.state;

    const honeyEditWindowBody = <form>
      <div className="form-group">
        <label htmlFor="honey-name" className="col-form-label">Название:</label>
        <input type="text" className="form-control" id="honey-name" value={honey.name} onChange={this.handleNameOnChange}></input>
      </div>
      <div className="form-group">
        <label htmlFor="honey-description" className="col-form-label">Описание:</label>
        <textarea className="form-control" id="honey-description" value={honey.description} onChange={this.handleDescriptionOnChange}></textarea>
      </div>
      <div className="form-group">
        <label htmlFor="honey-price" className="col-form-label">Цена:</label>
        <textarea className="form-control" id="honey-price" value={honey.price} onChange={this.handlePriceOnChange}></textarea>
      </div>
      <div className="form-group">
        <label htmlFor="honey-status" className="col-form-label">Статус:</label>
        <textarea className="form-control" id="honey-status" value={honey.status} onChange={this.handleStatusOnChange}></textarea>
      </div>
    </form>;

    const honeyEditWindowActions = [
      <button key="1" type="button" className="btn btn-primary" onClick={() => handleDelete(honey)} >Удалить</button>,
      <button key="2" type="button" className="btn btn-secondary" onClick={() => { handleSave(honey); handleClose() }} >Сохранить</button>
    ];

    return (
      <EditWindow
        windowTitle={honey.name}
        windowBody={honeyEditWindowBody}
        windowActions={honeyEditWindowActions}
        handleClose={handleClose}
      />
    );
  }
}

HoneyEditWindow.propTypes = {
  honey: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    price: PropTypes.number,
    status: PropTypes.string
  }),
  handleDelete: PropTypes.func.isRequired,
  handleSave: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default HoneyEditWindow;
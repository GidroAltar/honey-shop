import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ConsumerEditWindow from '../ConsumerEditWindow';
import ItemsTableRow from '../../common/ItemsTableRow';
import "./ConsumerRow.css";

class ConsumerRow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            consumerEditWindowVisible: false
        }
    }

    hideConsumerEditWindow = () => {
        this.setState({
            consumerEditWindowVisible: false
        })
    }

    showConsumerEditWindow = () => {
        this.setState({
            consumerEditWindowVisible: true
        })
    }

    render() {
        const { consumerEditWindowVisible } = this.state;
        const { consumer, deleteConsumer, saveConsumer } = this.props;

        const consumerFieldValues = [consumer.name];
        const itemActionElements = [
<button type="button" className="btn btn-primary consumer-items__action" onClick={this.showConsumerEditWindow}>Редактировать</button>,
                    <button type="button" className="btn btn-danger consumer-items__action" onClick={() => deleteConsumer(consumer)}>Удалить</button>
        ];
        return (
            <div>
                <ItemsTableRow
                    fieldValues={consumerFieldValues}
                    itemActionElements={itemActionElements}
                />
                {
                    (consumerEditWindowVisible)
                        ? <ConsumerEditWindow
                            consumer={consumer}
                            handleClose={this.hideConsumerEditWindow}
                            handleDelete={deleteConsumer}
                            handleSave={saveConsumer} />
                        : null
                }
            </div>
        );
    }
}

ConsumerRow.propTypes = {
    consumer: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string,
    }),
    deleteConsumer: PropTypes.func.isRequired,
    saveConsumer: PropTypes.func.isRequired,
};

export default ConsumerRow;
import React, { Component } from 'react';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import ConsumerRow from './ConsumerRow';
import ConsumerAddWindow from './ConsumerAddWindow';
import consumerActionCreators from '../../actions/consumerActions';

import "./Consumer.css";

class Consumer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      consumerAddWindowVisible: false
    }
  }

  hideConsumerAddWindow = () => {
    this.setState({
      consumerAddWindowVisible: false
    })
  }

  showConsumerAddWindow = () => {
    this.setState({
      consumerAddWindowVisible: true
    })
  }
  componentDidMount() {
    console.log("consumer did mount");
  }

  render() {
    console.log("consumer render");
    const { addConsumer, saveConsumer, deleteConsumer } = this.props.consumerActionCreators;
    const { consumerAddWindowVisible } = this.state;
    return (
      <main role="main" className="container">
        {
          (consumerAddWindowVisible)
            ? <ConsumerAddWindow
              handleClose={this.hideConsumerAddWindow}
              handleAdd={addConsumer} />
            : null
        }
        <p>
          <button type="button" className="btn btn-outline-primary" onClick={this.showConsumerAddWindow}>Добавить</button>
        </p>
            {
              this.props.consumers.map((iterConsumer) => 
              <ConsumerRow
                key={iterConsumer.id}
                consumer={iterConsumer}
                deleteConsumer={deleteConsumer}
                saveConsumer={saveConsumer} 
              />)
            }
      </main>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    consumerActionCreators: bindActionCreators(consumerActionCreators, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    consumers: state.consumers
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Consumer);
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import consumerActionCreators from '../../../actions/consumerActions';
import EditWindow from '../../common/EditWindow';

class ConsumerAddWindow extends Component {

  constructor(props) {
    super(props);
    this.state = {
        consumer: {
          name: "",
          description: "",
          email: "",
          telephone: ""
        }
    }
  }

  handleModalParentOnClick = (e) => {
    if (e.target === e.currentTarget) {
      this.props.handleClose();
    }
  }

  handleNameOnChange = (e) => {
    this.setState( {
      consumer: {...this.state.consumer, name: e.currentTarget.value }
    })
  }

  handleEmailOnChange = (e) => {
    this.setState( {
      consumer: {...this.state.consumer, email: e.currentTarget.value }
    })
  }

  handleTelephoneOnChange = (e) => {
    this.setState( {
      consumer: {...this.state.consumer, telephone: e.currentTarget.value }
    })
  }

  handleDescriptionOnChange = (e) => {
    this.setState( {
      consumer: {...this.state.consumer, description: e.currentTarget.value }
    })
  }

  render() {
    const { handleAdd, handleClose } = this.props;
    const { consumer } = this.state;

    const consumerEditWindowBody = <form>
    <div className="form-group">
      <label htmlFor="consumer-name" className="col-form-label">Название:</label>
      <input type="text" className="form-control" id="consumer-name" value = {consumer.name} onChange={ this.handleNameOnChange }></input>
    </div>
    <div className="form-group">
      <label htmlFor="consumer-email" className="col-form-label">Email:</label>
      <input type="text" className="form-control" id="consumer-email" value = {consumer.email} onChange={ this.handleEmailOnChange }></input>
    </div>
    <div className="form-group">
      <label htmlFor="consumer-telephone" className="col-form-label">Телефон:</label>
      <input type="text" className="form-control" id="consumer-telephone" value = {consumer.telephone} onChange={ this.handleTelephoneOnChange }></input>
    </div>
    <div className="form-group">
      <label htmlFor="description-text" className="col-form-label">Описание:</label>
      <textarea className="form-control" id="description-text" value = {consumer.description} onChange={ this.handleDescriptionOnChange }></textarea>
    </div>
  </form>;

  const consumerEditWindowActions = [
    <button key = "1" type="button" className="btn btn-secondary" onClick={ () => { handleAdd(consumer); handleClose()} } >Сохранить</button>
  ];

  return (
    <EditWindow
      windowTitle={"Заполните поля:"}
      windowBody={consumerEditWindowBody}
      windowActions={consumerEditWindowActions}
      handleClose={handleClose}
    />
    );
  }
}

ConsumerAddWindow.propTypes = {
  handleAdd: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default ConsumerAddWindow;
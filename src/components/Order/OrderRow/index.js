import React, { Component } from 'react';
import PropTypes from 'prop-types';

import OrderEditWindow from '../OrderEditWindow';
import ItemsTableRow from '../../common/ItemsTableRow';
import "./OrderRow.css";

class OrderRow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orderEditWindowVisible: false
        }
    }

    hideOrderEditWindow = () => {
        this.setState({
            orderEditWindowVisible: false
        })
    }

    showOrderEditWindow = () => {
        this.setState({
            orderEditWindowVisible: true
        })
    }

    render() {
        const { orderEditWindowVisible } = this.state;
        const { order, deleteOrder, saveOrder } = this.props;

        const orderFieldValues = [order.name];
        const itemActionElements = [
            <button type="button" className="btn btn-primary order-items__action" onClick={this.showOrderEditWindow}>Редактировать</button>,
            <button type="button" className="btn btn-danger order-items__action" onClick={() => deleteOrder(order)}>Удалить</button>
        ];
        return (
            <div>
                <ItemsTableRow
                    fieldValues={orderFieldValues}
                    itemActionElements={itemActionElements}
                />
                {
                    (orderEditWindowVisible)
                        ? <OrderEditWindow
                            order={order}
                            handleClose={this.hideOrderEditWindow}
                            handleDelete={deleteOrder}
                            handleSave={saveOrder} />
                        : null
                }
            </div>
        );
    }
}

OrderRow.propTypes = {
    order: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string,
        startDate: PropTypes.string,
        status: PropTypes.string
    }),
    deleteOrder: PropTypes.func.isRequired,
    saveOrder: PropTypes.func.isRequired,
};

export default OrderRow;
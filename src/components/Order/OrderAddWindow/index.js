import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import orderActionCreators from '../../../actions/orderActions';
import EditWindow from '../../common/EditWindow';

class OrderAddWindow extends Component {

  constructor(props) {
    super(props);
    this.state = {
        order: {
          name: "",
          description: "",
          status: "",
          startDate: ""
        }
    }
  }

  handleModalParentOnClick = (e) => {
    if (e.target === e.currentTarget) {
      this.props.handleClose();
    }
  }

  handleNameOnChange = (e) => {
    this.setState( {
      order: {...this.state.order, name: e.currentTarget.value }
    })
  }

  handleStatusOnChange = (e) => {
    this.setState( {
      order: {...this.state.order, status: e.currentTarget.value }
    })
  }

  handleDescriptionOnChange = (e) => {
    this.setState( {
      order: {...this.state.order, description: e.currentTarget.value }
    })
  }

  handleStartDateOnChange = (e) => {
    this.setState( {
      order: {...this.state.order, startDate: e.currentTarget.value }
    })
  }

  render() {
    const { handleAdd, handleClose } = this.props;
    const { order } = this.state;

    const orderEditWindowBody = <form>
    <div className="form-group">
      <label htmlFor="order-name" className="col-form-label">Название:</label>
      <input type="text" className="form-control" id="order-name" value = {order.name} onChange={ this.handleNameOnChange }></input>
    </div>
    <div className="form-group">
      <label htmlFor="order-status" className="col-form-label">Статус:</label>
      <input type="text" className="form-control" id="order-status" value = {order.status} onChange={ this.handleStatusOnChange }></input>
    </div>
    <div className="form-group">
      <label htmlFor="order-start-date" className="col-form-label">Дата создания:</label>
      <input type="text" className="form-control" id="order-status" value = {order.startDate} onChange={ this.handleStartDateOnChange }></input>
    </div>
    <div className="form-group">
      <label htmlFor="description-text" className="col-form-label">Описание:</label>
      <textarea className="form-control" id="description-text" value = {order.description} onChange={ this.handleDescriptionOnChange }></textarea>
    </div>
  </form>;

  const orderEditWindowActions = [
    <button key = "1" type="button" className="btn btn-secondary" onClick={ () => { handleAdd(order); handleClose()} } >Сохранить</button>
  ];

  return (
    <EditWindow
      windowTitle={"Заполните поля:"}
      windowBody={orderEditWindowBody}
      windowActions={orderEditWindowActions}
      handleClose={handleClose}
    />
    );
  }
}

OrderAddWindow.propTypes = {
  handleAdd: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default OrderAddWindow;
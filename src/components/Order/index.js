import React, { Component } from 'react';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import OrderRow from './OrderRow';
import OrderAddWindow from './OrderAddWindow'
import orderActionCreators from '../../actions/orderActions';

import "./Order.css";

class Order extends Component {

  constructor(props) {
    super(props);
    this.state = {
      orderAddWindowVisible: false
    }
  }

  hideOrderAddWindow = () => {
    this.setState({
      orderAddWindowVisible: false
    })
  }

  showOrderAddWindow = () => {
    this.setState({
      orderAddWindowVisible: true
    })
  }
  render() {
    const { addOrder, saveOrder, deleteOrder } = this.props.orderActionCreators;
    const { orderAddWindowVisible } = this.state;
    return (
      <main role="main" className="container">
        {(orderAddWindowVisible)
          ? <OrderAddWindow
            handleClose={this.hideOrderAddWindow}
            handleAdd={addOrder} />
          : null
        }
        <p>
          <button type="button" className="btn btn-outline-primary" onClick={this.showOrderAddWindow}>Добавить</button>
        </p>

        {this.props.orders.map((iterOrder) => <OrderRow key={iterOrder.id} order={iterOrder} deleteOrder={deleteOrder} saveOrder={saveOrder} />)}
      </main>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    orderActionCreators: bindActionCreators(orderActionCreators, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    orders: state.orders
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Order);
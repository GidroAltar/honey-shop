import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux";

import orderActionCreators from '../../../actions/orderActions';
import EditWindow from '../../common/EditWindow';

class OrderEditWindow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      order: props.order
    }
  }

  handleNameOnChange = (e) => {
    this.setState({
      order: { ...this.state.order, name: e.currentTarget.value }
    })
  }

  handleDescriptionOnChange = (e) => {
    this.setState({
      order: { ...this.state.order, description: e.currentTarget.value }
    })
  }

  handleStatusOnChange = (e) => {
    this.setState({
      order: { ...this.state.order, status: e.currentTarget.value }
    })
  }

  handleStartDateOnChange = (e) => {
    this.setState({
      order: { ...this.state.order, startDate: e.currentTarget.value }
    })
  }

  render() {
    const { handleDelete, handleSave, handleClose } = this.props;
    const { order } = this.state;

    const orderEditWindowBody = <form>
        <div className="form-group">
          <label htmlFor="order-name" className="col-form-label">Название:</label>
          <input type="text" className="form-control" id="order-name" value={order.name} onChange={this.handleNameOnChange}></input>
        </div>
        <div className="form-group">
          <label htmlFor="description-text" className="col-form-label">Описание:</label>
          <textarea className="form-control" id="description-text" value={order.description} onChange={this.handleDescriptionOnChange}></textarea>
        </div>
        <div className="form-group">
          <label htmlFor="start-date-text" className="col-form-label">Дата создания:</label>
          <textarea className="form-control" id="start-date-text" value={order.startDate} onChange={this.handleStartDateOnChange}></textarea>
        </div>
        <div className="form-group">
          <label htmlFor="status-text" className="col-form-label">Статус:</label>
          <textarea className="form-control" id="status-text" value={order.status} onChange={this.handleStatusOnChange}></textarea>
        </div>
    </form>;

    const orderEditWindowActions = [
      <button key = "1" type="button" className="btn btn-primary" onClick={() => handleDelete(order)} >Удалить</button>,
      <button key = "2" type="button" className="btn btn-secondary" onClick={() => { handleSave(order); handleClose() }} >Сохранить</button>
    ];

    return (
      <EditWindow
        windowTitle={order.name}
        windowBody={orderEditWindowBody}
        windowActions={orderEditWindowActions}
        handleClose={handleClose}
      />
    );
  }
}

OrderEditWindow.propTypes = {
  order: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    startDate: PropTypes.string,
    status: PropTypes.string
  }),
  handleDelete: PropTypes.func.isRequired,
  handleSave: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default OrderEditWindow;
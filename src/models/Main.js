export default class Honey {
    consturctor(name, description) {
        this.name = name;
        this.description = description;
    }

    id;
    name;
    description;
}

export default storeStructure = {
    common: {
        serverApp: {},
        appLoaded: true
    },
    entities: {
        honey: {
            1: { "id": 1, "name": "Липовый", "price": 550, "status": "Готов" },
            2: { "id": 2, "name": "Цветочный", "price": 350, "status": "Готов" }
        },
        provider: {},
        consumer: {},
        order: {}
    },
    pages: {
        honey: {
            isFetching: false,
            lastUpdated: 123234343,
            items: [1,2]
        },
        provider: {},
        consumer: {},
        order: {}
    }
}
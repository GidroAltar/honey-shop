export const APP_LOAD = "APP_LOAD";
export const SERVER_APP_INIT = "SERVER_APP_INIT";

export const ADD_CONSUMER = "ADD_CONSUMER";
export const SAVE_CONSUMER = "SAVE_CONSUMER";
export const DELETE_CONSUMER = "DELETE_CONSUMER";

export const ADD_HONEY = "ADD_HONEY";
export const SAVE_HONEY = "SAVE_HONEY";
export const DELETE_HONEY = "DELETE_HONEY";
export const RECEIVE_HONEY = 'RECEIVE_HONEY';
export const RECEIVE_HONEY_START = "RECEIVE_HONEY_START";
export const RECEIVE_HONEY_END = "RECEIVE_HONEY_END";

export const ADD_ORDER = "ADD_ORDER";
export const SAVE_ORDER = "SAVE_ORDER";
export const DELETE_ORDER = "DELETE_ORDER";

export const ADD_PROVIDER = "ADD_PROVIDER";
export const SAVE_PROVIDER = "SAVE_PROVIDER";
export const DELETE_PROVIDER = "DELETE_PROVIDER";
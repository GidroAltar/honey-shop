import * as cuba from '@cuba-platform/rest';
import { HONEY_ENTITY } from './constants/entitiesName';

// function parseEntitiesFromResponse(response) {
//     for (entityObject in response) {

//     }
// }

export const HoneyAPI = {
    post: (honey, app) => app.commitEntity(HONEY_ENTITY, honey),
    get: (id, app) =>  id,
    getAll: (app) => app.loadEntities(HONEY_ENTITY, {view: 'honey-view'}),
    delete: (app, id) => null
}